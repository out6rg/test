function validateForm() {

	var login = document.getElementById("login");
	var email = document.getElementById("email");
	var password = document.getElementById("password");
	var password_confirm = document.getElementById("password_confirm");
	var phone = document.getElementById("phone_number");
	var error;


	error = document.getElementById("login_error");
	if (login.value.length < 3) {
		error.innerHTML = "<p style=\"color: red;\">Wrong login (мин. 3 символа)</p>";
	} else {
		error.innerHTML = "";
	}
	//@\w\.\w
	error = document.getElementById("email_error");
	if (!email.value.match(/[-\w]+@[-\.\w]+\.[\w]+/g)) {

		error.innerHTML = "<p style=\"color: red;\">Wrong email</p>";


	} else {
		error.innerHTML = "";
	}
	//password.value.length < 6
	error = document.getElementById("password_error");
	if (password.value.length < 6) {


		error.innerHTML = "<p style=\"color: red;\">пароль мин. 6 символов</p>";

	} else {

		error.innerHTML = "";

	}
	// !password_confirm.match(password)
	//password_confirm!==password
	error = document.getElementById("password_confirm_error");
	if (password_confirm.value != password.value) {
		error.innerHTML = "<p style=\"color: red;\">Пароли не совпадают</p>";

	} else {
		error.innerHTML = "";
	}

	error = document.getElementById("phone_number_error");
	if (!phone.value.match(/^(8|\+7)\(\d{3}\)\d{3}\-\d{2}\-\d{2}/)) {

		error.innerHTML = "<p style=\"color: red;\">Формат телеофна:</p> <p style=\"color: red;\">+7(***)***-**-**</p> <p style=\"color: red;\">8(***)***-**-**</p> ";


	} else {
		error.innerHTML = "";
	}


	return false;
}